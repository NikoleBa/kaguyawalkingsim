﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GPPR
{
    

    public class LoadAccept : MonoBehaviour
    {
        public string scene;
    
        public void LoadingAccept()
        {
            SceneManager.LoadScene(scene);
        }
    }
}