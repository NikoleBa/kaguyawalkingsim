﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{
    

    public class Singing : MonoBehaviour
    {
        [SerializeField]
        private AudioClip sing;
        private AudioSource audioSource;

        public bool isSinging;

        // In the Awake Method we assign audioSource to an Component of the Child in this case AudioSource so we dont have to "drag and drop"
        // We our Audioclip to the Audiosource so that it plays when we want to
        private void Awake()
        {
            audioSource = GetComponent<AudioSource>();

            audioSource.clip = sing;
            audioSource.time = 0f;
        }

        // In the Update Method we have IF statement for Playing and Stopping the AudioClip we have
        // The isSinging bool is for the Visibility Script   
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                audioSource.Play();
            }
            if(audioSource.time > 10f)
            {
                audioSource.Stop();
            }

            if (audioSource.isPlaying)
            {
                isSinging = true;
            }
            else
            {
             isSinging = false;
            }
        }
    }
}