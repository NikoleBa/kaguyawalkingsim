﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GPPR
{
    

    public class Interactable : MonoBehaviour
    {
        public GameObject canvas;
        public bool isInRange;
        public UnityEvent interactAction;

        private string checkTag = "Player";

        // We assign a Canvas to so we can activate/deactivate it
        void Start()
        {
            if(canvas == null)
            {
                canvas = GameObject.FindWithTag("DialogCanvas");
            }
        }

        // here we allow the Player to Interact with Interactable Objects if the player is in Range and presses E on the Keyboard.
        void Update()
        {
            if (isInRange)
            {

                if (Input.GetKeyDown(KeyCode.E))
                {
                    canvas.SetActive(true);
                    interactAction.Invoke();
                }
            }
        }

        // this Method Checks if the Player enters the BoxCollider we put in
        private void OnTriggerEnter(Collider other)
        {
        
            if (other.gameObject.CompareTag(checkTag))
            {
                isInRange = true;
            }
        }

        //this Method Checks if the Player leaves the BoxCollider
        private void OnTriggerExit(Collider other)
        {
            isInRange = false;
        }
    }
}