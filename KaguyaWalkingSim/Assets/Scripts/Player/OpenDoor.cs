﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{
    

    public class OpenDoor : MonoBehaviour

    {
        private bool isInRange = false;
        private Animator animator;
        public AudioSource openDoor;
        // Start is called before the first frame update
        void Start()
        {
            animator = GetComponent<Animator>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player")
            {
                isInRange = true;
                Debug.Log("Open up");
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.tag =="Player")
            {
                isInRange = false;
            }
        }
        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.E) && isInRange)
            {
                animator.SetBool("Open", true);
                playSound();
            }
        }

        private void playSound()
        {
            openDoor.Play();
        }
    }
}