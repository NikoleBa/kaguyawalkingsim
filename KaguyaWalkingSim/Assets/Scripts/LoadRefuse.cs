﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GPPR
{
    public class LoadRefuse : MonoBehaviour
    {
        public string scene;
    
        public void LoadingRefuse()
        {
            SceneManager.LoadScene(scene);
        }
    }
}