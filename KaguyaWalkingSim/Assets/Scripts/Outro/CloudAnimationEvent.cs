﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GPPR
{
    public class CloudAnimationEvent : MonoBehaviour
    {
        public GameOutroManager outroManager;

        public void EndScene()
        {
            outroManager.ShowEndingText();
        }
    }
}