﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace GPPR
{
    public class GameOutroManager : MonoBehaviour
    {
        public TextMeshProUGUI displayText;

        public Animator cloudAnimator;

        public float typingSpeed = 0.05f;
        
        [TextArea(5, 5)]
        public string outroText;

        [TextArea(5, 5)]
        public string endingText;

        public GameObject textPanel;

        public float blockInputSeconds = 5.0f;

        public Image panelBlack;

        [SerializeField]
        private bool allowReturnToMenu = false;

        void Update()
        {
            if (Input.anyKeyDown && blockInputSeconds <= 0)
            {
                StartCloudAnimation();
                return;
            }
            else if (Input.anyKeyDown && allowReturnToMenu)
            {
                FindObjectOfType<OutroSceneChanger>().FadeOut("Menu");
                return;
            }

            if (blockInputSeconds > 0)
                blockInputSeconds -= Time.deltaTime;
        }

        public void DisplayText()
        {
            textPanel.SetActive(true);
            StartCoroutine(TypeSentence(outroText));
        }

        private IEnumerator TypeSentence(string sentence)
        {
            foreach (char letter in sentence.ToCharArray())
            {
                displayText.text += letter;
                yield return new WaitForSeconds(typingSpeed);
            }
        }

        void StartCloudAnimation()
        {
            cloudAnimator.SetTrigger("Click");
        }

        public void ShowEndingText()
        {
            typingSpeed /= 2;
            displayText.alignment = TextAlignmentOptions.Center;

            StopAllCoroutines();
            panelBlack.gameObject.SetActive(true);
            StartCoroutine(FadeOut());
        }

        IEnumerator FadeOut()
        {
            for (float alpha = 0; alpha <= 1; alpha += Time.deltaTime)
            {
                panelBlack.color = new Color(0, 0, 0, alpha);
                yield return null;
            }

            displayText.text = "";
            StartCoroutine(TypeSentence(endingText));

            allowReturnToMenu = true;
        }
    }
}