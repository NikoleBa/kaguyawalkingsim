﻿using System;
using System.Collections;
using UnityEngine;

namespace GPPR
{
    public class FadeObject : MonoBehaviour
    {
        private float fadeSpeed =.2f;
        public float FadeSpeed { get => fadeSpeed; set => fadeSpeed = value; }

        [SerializeField]
        private Renderer objectRenderer;

        private void Start()
        {
            objectRenderer = GetComponentInChildren<Renderer>();
        }

        public void FadeIn()
        {
            StartCoroutine(FadeInObject()); ;
        }

        public void FadeOut()
        {
            StartCoroutine(FadeOutObject());
        }

        public IEnumerator FadeOutObject()
        {
            while (objectRenderer.material.color.a > 0)
            { 
                Color objectColor = objectRenderer.material.color;
                float alpha = objectColor.a;
                float fadeAmount = alpha - (FadeSpeed * Time.deltaTime);

                objectColor = new Color(objectColor.r, objectColor.r, objectColor.g, fadeAmount);
                objectRenderer.material.color = objectColor;

                yield return null;
            }
        }

        public IEnumerator FadeInObject()
        {
            while (objectRenderer.material.color.a < 1)
            { 
                Color objectColor = objectRenderer.material.color;
                float alpha = objectColor.a;
                float fadeAmount = alpha + (FadeSpeed * Time.deltaTime);

                objectColor = new Color(objectColor.r, objectColor.r, objectColor.g, fadeAmount);
                objectRenderer.material.color = objectColor;

                yield return null;
            }
        }
    }
}