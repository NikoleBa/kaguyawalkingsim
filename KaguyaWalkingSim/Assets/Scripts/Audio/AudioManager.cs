﻿using UnityEngine.Audio;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GPPR
{
    public class AudioManager : MonoBehaviour
    {
        public GameObject audioManager;
        public static AudioManager instance;
        
        void Awake()
        {
            audioManager = GameObject.FindGameObjectWithTag("AudioManager");
            
            DontDestroyOnLoad(gameObject);
        
            if (instance == null)
            {
                instance = this;
            }

            else
            {
                Destroy(audioManager);
            }
        }
    }
}