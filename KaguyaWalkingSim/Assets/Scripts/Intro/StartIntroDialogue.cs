﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{
public class StartIntroDialogue : DialogueTrigger
{
    public DialogueManager dialogueManager;
    

    private void Start()
    {
       TriggerDialogue();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Mouse0) || Input.GetKeyDown(KeyCode.Return))
        {
            Debug.Log("Button was pressed");
            dialogueManager.DisplayNextSentence();
            

        }
    }
}
}
