﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{
    public class IntroGameObjectCheck : MonoBehaviour
    {

        public GameObject[] keys;
        public GameObject[] antikeys;
        public GameObject[] enableThis;
        public GameObject[] disableThis;

        private bool allInactive = false;
        private bool allActive = false;

        void Update()
        {
            KeyCheck();
        }

        public void KeyCheck()
        {
            // here we check if all the Objects in the Array are disabled
            for (int i = 0; i < keys.Length; i++)
            {
                if (keys[i].activeInHierarchy == true)
                {
                    allInactive = false;
                
                }
                else
                {
                    allInactive = true;
                }
            }

            for (int i = 0; i < antikeys.Length; i++)
            {
                if (antikeys[i].activeInHierarchy == false)
                {
                    allActive = false;

                }
                else
                {
                    allActive = true;
                }
            }





            //if allInactive is true then all Objects in the enableThis Array get activated
            for (int i = 0; i < enableThis.Length; i++)
            {
                if (allInactive == true || allActive == true)
                {
                    enableThis[i].SetActive(true);
                }
            }
            //similiar as above exept it does the opposite: all Objects get disabled
            for (int i = 0; i < disableThis.Length; i++)
            {
                if (allInactive == true || allActive == true)
                {     
                    {
                        disableThis[i].SetActive(false);
                    
                    }            
                }
            }
        }
    }
}