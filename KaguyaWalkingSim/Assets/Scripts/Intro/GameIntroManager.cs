﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace GPPR
{
    public class GameIntroManager : MonoBehaviour
    {
        public GameObject[] frames;

        public float typingSpeed = 0.05f;


        private int currentFrameIndex = -1;

        void Start()
        {
            NextFrame();
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.anyKeyDown)
            {
                if (frames[currentFrameIndex].GetComponent<FadeOnClick>().IsFading())
                {
                    return;
                }
                frames[currentFrameIndex].GetComponent<FadeOnClick>().FadeOut();
            }
        }

        private IEnumerator TypeSentence(string sentence)
        {
            foreach (char letter in sentence.ToCharArray())
            {
                frames[currentFrameIndex].GetComponentInChildren<TextMeshProUGUI>().text += letter;
                yield return new WaitForSeconds(typingSpeed);
            }
        }

        public void NextFrame()
        {
            currentFrameIndex++;
            if (currentFrameIndex < frames.Length && frames[currentFrameIndex] != null)
            {
                frames[currentFrameIndex].SetActive(true);
                frames[currentFrameIndex].GetComponent<FadeOnClick>().FadeIn();
                string thetext = frames[currentFrameIndex].GetComponent<IntroFrame>().text;
                StopAllCoroutines();
                StartCoroutine(TypeSentence(thetext));
            }
            else
            {
                SceneManager.LoadScene("Anleitung");
            }
        }
    }
}