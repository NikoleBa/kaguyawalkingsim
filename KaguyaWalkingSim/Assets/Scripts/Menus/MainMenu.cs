﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GPPR
{
    public class MainMenu : MonoBehaviour
    {
        public GameObject credits;
        
        public void PlayGame()
        {
            //loads the next Scene in the queue
            SceneManager.LoadScene("Intro");
        }
        public void loadingCredits()
        {
            credits.SetActive(true);
        }

        public void unloadingCredits()
        {
            credits.SetActive(false);
        }

        public void QuitGame()
        {
            //Quits the Game,duh
            Application.Quit();
            Debug.Log("You've hit the Quit-Button succesfully");
        }
    }
}