﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GPPR
{ 
    public class LoadMenu : MonoBehaviour
    {
        public void LoadingMenu()
        {
            SceneManager.LoadScene("Menu");
        }
    }
}