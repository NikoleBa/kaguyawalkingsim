﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GPPR
{
    public class PauseMenu : MonoBehaviour
    {
        public static bool GameIsPaused = false;
        public GameObject pauseMenuUI;
        public GameObject Panel;
        public GameObject audioOverlay;
        public GameObject mainmenuOverlay;
        public GameObject quitOverlay;

        
        void Update()
        {
               if(pauseMenuUI.activeInHierarchy == true)
               {
                        Cursor.visible = true;
                        Cursor.lockState = CursorLockMode.Confined;
               }
            
               if (Input.GetKeyDown(KeyCode.Escape)|| Input.GetKeyDown(KeyCode.P))
               {
                    if (GameIsPaused)
                    {
                        Resume();
                    }

                    else
                    {
                        Pause(); 
                    }
               }
        }

        public void Resume()
        {
            pauseMenuUI.SetActive(false);
            Time.timeScale = 1f;
            GameIsPaused = false;
            Panel.SetActive(false);
            audioOverlay.SetActive(false);
            mainmenuOverlay.SetActive(false);
            quitOverlay.SetActive(false);
        }

        void Pause()
        {
            pauseMenuUI.SetActive(true);
            Time.timeScale = 0f;
            GameIsPaused = true;
            Cursor.visible = true;
        
        }
        
        public void EnableAudio() 
        {
            Panel.SetActive(true);
            audioOverlay.SetActive(true);
            
        }
        
        public void DisableAudio() 
        {
            Panel.SetActive(false);
            audioOverlay.SetActive(false);
            
        }
        
        

        public void EnableLoadMenu() 
        {
            Panel.SetActive(true);
            mainmenuOverlay.SetActive(true);
        }
        
        public void DisableLoadMenu() 
        {
            Panel.SetActive(false);
            mainmenuOverlay.SetActive(false);
        }
        public void LoadMenu()
        {
            Time.timeScale = 1f;
            SceneManager.LoadScene("Menu");
        }
        
        
        public void EnableQuit() 
        {
            Panel.SetActive(true);
            quitOverlay.SetActive(true);
        }
        
        public void DisableQuit() 
        {
            Panel.SetActive(false);
            quitOverlay.SetActive(false);
        }

        public void QuitGame()
        {
            Debug.Log("Spiel beendet....");
            Application.Quit();
        }
    }
}