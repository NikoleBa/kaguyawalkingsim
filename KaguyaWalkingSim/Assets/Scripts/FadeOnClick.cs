﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GPPR
{
    public class FadeOnClick : MonoBehaviour
    {
        public Animator animator;


        [SerializeField]
        private bool isFading = false;
   
    
        void Start()
        {

        }

        public void FadeIn()
        {
            isFading = true;
            animator.SetTrigger("FadeIn");
        }

        public void FadeOut()
        {
            isFading = true;
            animator.SetBool("continueFade", true);
        }

        public void AfterFadeIn()
        {
            isFading = false;
        }

        public void AfterFadeOut()
        {
            isFading = false;
            FindObjectOfType<GameIntroManager>().NextFrame();
            Destroy(gameObject);
        }

        public bool IsFading()
        {
            return isFading;
        }
    }
}