﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GPPR
{
    

    public class SceneChanger : MonoBehaviour
    {
        private string checkTag = "Player";
        public bool isInRange;
        public string scene;

        // Update is called once per frame
        void Update()
        {
            if (isInRange == true)
            {
                SceneManager.LoadScene(scene);
            }
        }

        private void OnTriggerEnter(Collider other)
        {

            if (other.gameObject.CompareTag(checkTag))
            {
                isInRange = true;
            }
        }
    }
}