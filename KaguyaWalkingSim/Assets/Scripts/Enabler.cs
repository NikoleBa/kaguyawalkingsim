﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{

public class Enabler : MonoBehaviour
{
    //Old Script we dont use anymore
    //We Combined this Script with the Dialouge Script because we want stuff to happen AFTER a Dialogue ends



     //Zum Deactivieren des Objectes
    public GameObject toDisable;
    public bool shouldDisable;

    //Zum Aktivieren von Objecten
    public GameObject toEnable;
    public bool shouldEnable;

    //Für Spawning von neuen GameObjecten
    public GameObject toSpawn;
    public Transform spawnPoint;
    public bool shouldSpawn;

    public DialogueManager dialougeManager;

    public void Update()
    {
        if(dialougeManager.isFinished == true)
        {
            //DoOtherThings();
        }
    }

    void DoOtherThings()
    {

                //Für Spawning
        if(shouldSpawn == true)
        {
            dialougeManager.index = 1;
            dialougeManager.toSpawn = toSpawn;
            dialougeManager.spawnPoint = spawnPoint;
        }
        
        if(shouldEnable == true)
        {
            dialougeManager.index = 2;
            dialougeManager.toEnable = toEnable;
        }

        if(shouldDisable == true)
        {
            dialougeManager.index = 3;
            dialougeManager.toDisable = toDisable;
        }

        else
        {
            dialougeManager.index = 0;
        }
    }

}
}
