﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace GPPR
{
    public class OutroSceneChanger : MonoBehaviour
    {
        public Animator sceneFadeAnim;
        public GameObject fadeMask;

        public GameOutroManager outroManager;

        private string sceneName;

        void Start()
        {
            fadeMask.SetActive(true);
        }

        public void FadeOut()
        {
            this.sceneName = null;
            this.sceneFadeAnim.SetTrigger("FadeOut");
        }

        public void FadeOut(string toScene)
        {
            this.sceneName = toScene;
            this.sceneFadeAnim.SetTrigger("FadeOut");
        }

        private void OnFadeInComplete()
        {
            outroManager.DisplayText();
        }

        private void OnFadeComplete()
        {
            if (sceneName == null)
            {
                // show ending text
                outroManager.ShowEndingText();
            }
            else
            {
                SceneManager.LoadScene(sceneName);
            }
        }
    }
}
