﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{
    

    public class Visibility : MonoBehaviour
    {
        public bool isInRange;
        public bool isVisible = false;
        public Renderer[] skinnedMeshRenderer;
        public Behaviour halo;

        private string checkTag = "Player";

        public Singing singing;

        // Here we Assign our Array the Renderer of the Child Components
        void Awake()
        {
            skinnedMeshRenderer = GetComponentsInChildren<SkinnedMeshRenderer>();
        }

        void Update()
        {

            for (int i = 0; i < skinnedMeshRenderer.Length; i++)
            {
                // here we check if the Player is in Range and if the player is "singing"
                //If both Conditions are True, then the Renderer of the Rabbits get enabled aka. We can see them now.
                //the isSinging bool comes from the Singing Script attached to the Player
                if (isInRange == true && singing.isSinging == true || isVisible == true)
                {
                    skinnedMeshRenderer[i].enabled = true;
                    //halo.enabled = true;
                    isVisible = true;
                    GetComponent<FadeObject>().FadeIn();
                }

                else
                {
                    //skinnedMeshRenderer[i].enabled = false;
                    //halo.enabled = false;
                    GetComponent<FadeObject>().FadeOut();
                }
            }
        }

        // this Method Checks if the Player enters the BoxCollider we put in
        private void OnTriggerEnter(Collider other)
        {

            if (other.gameObject.CompareTag(checkTag))
            {
                isInRange = true;
            }
        }
        //this Method Checks if the Player leaves the BoxCollider
        private void OnTriggerExit(Collider other)
        {
                isInRange = false;
        }
    }
}