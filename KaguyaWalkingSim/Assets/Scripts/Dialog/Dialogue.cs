﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{
    [System.Serializable]
    public class Dialogue
    {
        //Array containing the sentences. public access modifier to make it accesible in unity
        public Sentence[] sentences;
    }
}