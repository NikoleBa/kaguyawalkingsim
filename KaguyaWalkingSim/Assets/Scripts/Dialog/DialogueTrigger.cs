﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{

    public class DialogueTrigger : MonoBehaviour
    {
        public DialogueManager dialougeManager;


        //Zum Aktivieren von Objecten
        public GameObject toEnable;
        public bool shouldEnable;

        //Zum Deactivieren des Objectes
        public GameObject toDisable;
        public bool shouldDisable;

        //Für Spawning von neuen GameObjecten
        public GameObject toSpawn;
        public Transform spawnPoint;
        public bool shouldSpawn;

        public Dialogue dialogue;

        public void Update()
        {
            if (dialougeManager == null)
            {
                dialougeManager = FindObjectOfType<DialogueManager>();
            }
        }

        public void TriggerDialogue()
        {
            FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
            DoOtherThings();
        }


        //If Abfrage um Sachen zu enablen/disablen/spawnen
        void DoOtherThings()
        {
            dialougeManager.shouldSpawn = false;
            dialougeManager.shouldEnable = false;
            dialougeManager.shouldDisable = false;

            //Für Spawning
            if (shouldSpawn == true)
            {
                dialougeManager.shouldSpawn = true;
                dialougeManager.toSpawn = toSpawn;
                dialougeManager.spawnPoint = spawnPoint;
            }
            if (shouldEnable == true)
            {
                dialougeManager.shouldEnable = true;
                dialougeManager.toEnable = toEnable;
            }

            if (shouldDisable == true)
            {
                dialougeManager.shouldDisable = true;
                dialougeManager.toDisable = toDisable;
            }

            else
            {
                dialougeManager.index = 0;
            }
        }
    }
}