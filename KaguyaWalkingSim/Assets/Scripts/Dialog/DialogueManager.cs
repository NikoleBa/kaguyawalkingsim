﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace GPPR
{

    public class DialogueManager : MonoBehaviour
    {
        public GameObject Canvas;
        public TextMeshProUGUI nameText;
        public TextMeshProUGUI dialogueText;
        public bool isFinished;
        public int index;

        //Diese Variablen sind zum Spawnen von Objecten
        public GameObject toSpawn;
        public Transform spawnPoint;
        public bool shouldSpawn;
        //Diese Veriablen sind zum Enablen von Objecten
        public GameObject toEnable;
        public bool shouldEnable;
        //Diese Variablen sind zum Disablen von Objecten
        public GameObject toDisable;
        public bool shouldDisable;

        //Order of sentences
        private Queue<Sentence> sentenceQueue;

        private void Awake()
        {
            sentenceQueue = new Queue<Sentence>();
        }

        public virtual void StartDialogue(Dialogue dialogue)
        {
            sentenceQueue.Clear();

            //Fill Queue with sentences
            foreach (Sentence sentence in dialogue.sentences)
            {
                sentenceQueue.Enqueue(sentence);
            }

            DisplayNextSentence();
        }
        
        public void DisplayNextSentence()
        {
            //end dialogue if queue is empty
            if (sentenceQueue.Count == 0)
            {
                EndDialogue();
                isFinished = true;
                BoolAbfrage();
                return;
            }
            //get the next sentence in queue
            Sentence sentence = sentenceQueue.Dequeue();
            nameText.text = sentence.speaker;
            dialogueText.text = sentence.sentence;
        }

        //This method can be overwritten by children 
        public virtual void EndDialogue()
        {
            Canvas.SetActive(false);
            Debug.Log("End of Dialogue");
        }
        public void BoolAbfrage()
        {
            if (shouldSpawn == true)
            {
                Instantiate(toSpawn, spawnPoint.position, Quaternion.identity);
            }

            if (shouldEnable == true)
            {
                toEnable.SetActive(true);
            }

            if (shouldDisable == true)
            {
                toDisable.SetActive(false);
            }
        }
    }
}