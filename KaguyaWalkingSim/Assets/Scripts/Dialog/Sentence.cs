﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{

    //container for text (speaker name, text dialogue) made accessible in unity
    [System.Serializable]
    public class Sentence
    {
        public string speaker;
        [TextArea(3, 10)]
        public string sentence;
    }
}