﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GPPR
{
    public class ImageFade : MonoBehaviour
    {
        public Animator animator;

        //how long the animation should wait
        public float waitSeconds = 5;
        [SerializeField]
        private float timer;
        [SerializeField]
        private bool isFadingIn = true;
   
    
        void Start()
        { //timed by waitseconds
            timer = waitSeconds;

        }

        void Update()
        { //Start the countdown after image is done fading in
            if(!isFadingIn)
            { 
                timer -= Time.deltaTime;

                //if timer is 0, image fades
                    if(timer <= 0)
                    {
                         animator.SetBool("continueFade", true);
                    }
            }
        }

        public void AfterFadeOut()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }

        //Is the image currently fading?
        public void FadeInFinished()
        {
            isFadingIn = false;
            Debug.Log("Im done fading");
        }
    }
}