﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{
    public class GameObjectCheck : MonoBehaviour
    {

        public GameObject[] keys; 
        public GameObject[] enableThis;
        public GameObject[] disableThis;

        private bool allInactive;

        void Update()
        {
            KeyCheck();
        }

        public void KeyCheck()
        {
            // here we check if all the Objects in the Array are disabled
            for (int i = 0; i < keys.Length; i++)
            {

                if (keys[i].activeInHierarchy)
                {
                    allInactive = false;
                    break;
                }
                else
                {
                    allInactive = true;
                }
            }
            //if allInactive is true then all Objects in the enableThis Array get activated
            for (int i = 0; i < enableThis.Length; i++)
            {
                if (allInactive == true)
                {
                    enableThis[i].SetActive(true);
                }
            }
            //similiar as above exept it does the opposite: all Objects get disabled
            for (int i = 0; i < disableThis.Length; i++)
            {
                if (allInactive == true)
                {     
                    {
                        disableThis[i].SetActive(false);
                    
                    }            
                }
            }
        }
    }
}