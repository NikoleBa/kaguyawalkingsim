﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{

    public class OutlineRange : MonoBehaviour
    {

        //If Player is in range, set the shader materual color to blue-ish
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                GetComponent<MeshRenderer>().materials[0].color = new Color(37f, 181f, 226f);
            }
        }

        //If Player leaves range, set it to gray
        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                GetComponent<MeshRenderer>().materials[0].color = new Color(56f, 56f, 56f);
            }
        }
    }

}

